#!/usr/bin/python
# test gui for rgbled

import pygtk
pygtk.require('2.0')
import gtk
import struct


class mygui:

    # Our new improved callback.  The data passed to this method
    # is printed to stdout.
    def callback(self, widget, data):
        val1r = self.led1r.get_value()
        val1g = self.led1g.get_value()
        val1b = self.led1b.get_value()

        val2r = self.led2r.get_value()
        val2g = self.led2g.get_value()
        val2b = self.led2b.get_value()

        val3r = self.led3r.get_value()
        val3g = self.led3g.get_value()
        val3b = self.led3b.get_value()

        val4r = self.led4r.get_value()
        val4g = self.led4g.get_value()
        val4b = self.led4b.get_value()


        print "value = %i%i%i " %( val1r,  val1g, val1b)
        fp.write(struct.pack("12B", val1r, val1g, val1b, val2r, val2g, val2b, val3r, val3g, val3b, val4r, val4g, val4b ))

    def delete_event(self, widget, event, data=None):
        gtk.main_quit()
        return False

    def __init__(self):
        # Create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title("RGB test")

        self.window.connect("delete_event", self.delete_event)

        # Sets the border width of the window.
        self.window.set_border_width(10)


        self.box1 = gtk.HBox(False, 0)
        self.window.add(self.box1)

        # led 1
        frame = gtk.Frame("Led 1")
        hbox = gtk.HBox()

        adjustment = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led1r = gtk.VScale(adjustment)
        self.led1r.set_digits(0)
        self.led1r.connect("value_changed", self.callback, "1")
        hbox.add(self.led1r)
        self.led1r.show()

        adjustment = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led1g = gtk.VScale(adjustment)
        self.led1g.set_digits(0)
        self.led1g.connect("value_changed", self.callback, "2")
        hbox.add(self.led1g)
        self.led1g.show()

        adjustment = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led1b = gtk.VScale(adjustment)
        self.led1b.set_digits(0)
        self.led1b.connect("value_changed", self.callback, "3")
        hbox.add(self.led1b)
        self.led1b.show()

        frame.add(hbox)
        hbox.show()
        self.box1.pack_start(frame, True, True, 0)
        frame.show()


        # led 2
        frame = gtk.Frame("Led 2")
        hbox = gtk.HBox()

        adjustment2r = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led2r = gtk.VScale(adjustment2r)
        self.led2r.set_digits(0)
        self.led2r.connect("value_changed", self.callback, "")
        hbox.add(self.led2r)
        self.led2r.show()

        adjustment2g = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led2g = gtk.VScale(adjustment2g)
        self.led2g.set_digits(0)
        self.led2g.connect("value_changed", self.callback, "")
        hbox.add(self.led2g)
        self.led2g.show()

        adjustment2b = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led2b = gtk.VScale(adjustment2b)
        self.led2b.set_digits(0)
        self.led2b.connect("value_changed", self.callback, "")
        hbox.add(self.led2b)
        self.led2b.show()

        frame.add(hbox)
        hbox.show()
        self.box1.pack_start(frame, True, True, 0)
        frame.show()


        # led 3
        frame = gtk.Frame("Led 3")
        hbox = gtk.HBox()

        adjustment3r = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led3r = gtk.VScale(adjustment3r)
        self.led3r.set_digits(0)
        self.led3r.connect("value_changed", self.callback, "")
        hbox.add(self.led3r)
        self.led3r.show()

        adjustment3g = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led3g = gtk.VScale(adjustment3g)
        self.led3g.set_digits(0)
        self.led3g.connect("value_changed", self.callback, "")
        hbox.add(self.led3g)
        self.led3g.show()

        adjustment3b = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led3b = gtk.VScale(adjustment3b)
        self.led3b.set_digits(0)
        self.led3b.connect("value_changed", self.callback, "")
        hbox.add(self.led3b)
        self.led3b.show()

        frame.add(hbox)
        hbox.show()
        self.box1.pack_start(frame, True, True, 0)
        frame.show()


        # led 4
        frame = gtk.Frame("Led 4")
        hbox = gtk.HBox()

        adjustment4r = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led4r = gtk.VScale(adjustment4r)
        self.led4r.set_digits(0)
        self.led4r.connect("value_changed", self.callback, "")
        hbox.add(self.led4r)
        self.led4r.show()

        adjustment4g = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led4g = gtk.VScale(adjustment4g)
        self.led4g.set_digits(0)
        self.led4g.connect("value_changed", self.callback, "")
        hbox.add(self.led4g)
        self.led4g.show()

        adjustment4b = gtk.Adjustment(value=0, lower=0, upper=255, step_incr=1, page_incr=0, page_size=0)
        self.led4b = gtk.VScale(adjustment4b)
        self.led4b.set_digits(0)
        self.led4b.connect("value_changed", self.callback, "")
        hbox.add(self.led4b)
        self.led4b.show()

        frame.add(hbox)
        hbox.show()
        self.box1.pack_start(frame, True, True, 0)
        frame.show()

        self.box1.show()
        self.window.show()

def main():
   
    gtk.main()

#if __name__ == "__main__":

fp = open("/dev/ttyACM0", "wb",0)
#fp = open("/tmp/test", "wb",0)
 
mygui()
main()
