/* rgb led usb firmware */
/* Tomi Leppikangas 11.1.2012*/


/* usb-serial code from LUFA: */

/*
             LUFA Library
     Copyright (C) Dean Camera, 2009.
              
  dean [at] fourwalledcubicle [dot] com
      www.fourwalledcubicle.com
*/

/*
  Copyright 2009  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, and distribute this software
  and its documentation for any purpose and without fee is hereby
  granted, provided that the above copyright notice appear in all
  copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaim all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/power.h>

#include "Descriptors.h"
#include <LUFA/Version.h>
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Drivers/USB/Class/CDC.h>

/* debug with simavr 
//#include "avr_mcu_section.h"

//FILE lcd_str = FDEV_SETUP_STREAM(lcd_putchar, NULL, _FDEV_SETUP_WRITE);

//AVR_MCU(F_CPU, "atmega32u2");
const struct avr_mmcu_vcd_trace_t _mytrace[]  _MMCU_ = {
   { AVR_MCU_VCD_SYMBOL("PORTD"), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led1R"), .mask = (1<<0), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led1G"), .mask = (1<<1), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led1B"), .mask = (1<<2), .what = (void*)&PORTD, },  
   
   { AVR_MCU_VCD_SYMBOL("Led2R"), .mask = (1<<3), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led2G"), .mask = (1<<4), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led2B"), .mask = (1<<5), .what = (void*)&PORTD, },  
   
   { AVR_MCU_VCD_SYMBOL("Led3R"), .mask = (1<<6), .what = (void*)&PORTD, },  
   { AVR_MCU_VCD_SYMBOL("Led3G"), .mask = (1<<0), .what = (void*)&PORTB, },  
   { AVR_MCU_VCD_SYMBOL("Led3B"), .mask = (1<<4), .what = (void*)&PORTB, },  
   
   { AVR_MCU_VCD_SYMBOL("Led4R"), .mask = (1<<6), .what = (void*)&PORTB, },  
   { AVR_MCU_VCD_SYMBOL("Led4G"), .mask = (1<<7), .what = (void*)&PORTB, },  
   { AVR_MCU_VCD_SYMBOL("Led4B"), .mask = (1<<5), .what = (void*)&PORTB, },  
};
*/

/* turn led on */
#define SET_1R PORTD |= (1<<0)
#define SET_1G PORTD |= (1<<1)
#define SET_1B PORTD |= (1<<2)

#define SET_2R PORTD |= (1<<3)
#define SET_2G PORTD |= (1<<4)
#define SET_2B PORTD |= (1<<5)

#define SET_3R PORTD |= (1<<6)
#define SET_3G PORTB |= (1<<0)
#define SET_3B PORTB |= (1<<4)

#define SET_4R PORTB |= (1<<6)
#define SET_4G PORTB |= (1<<7)
#define SET_4B PORTB |= (1<<5)

/* turn led off */
#define CLR_1R PORTD &= ~(1<<0)
#define CLR_1G PORTD &= ~(1<<1)
#define CLR_1B PORTD &= ~(1<<2)

#define CLR_2R PORTD &= ~(1<<3)
#define CLR_2G PORTD &= ~(1<<4)
#define CLR_2B PORTD &= ~(1<<5)

#define CLR_3R PORTD &= ~(1<<6)
#define CLR_3G PORTB &= ~(1<<0)
#define CLR_3B PORTB &= ~(1<<4)

#define CLR_4R PORTB &= ~(1<<6)
#define CLR_4G PORTB &= ~(1<<7)
#define CLR_4B PORTB &= ~(1<<5)



/* led intensity */
uint8_t led1r=0,
   led1g=0,
   led1b=0,
   led2r=0,
   led2g=0,
   led2b=0,
   led3r=0,
   led3g=0,
   led3b=0,
   led4r=0,
   led4g=0,
   led4b=0;

volatile uint8_t count=0;

static CDC_LineEncoding_t LineEncoding = { .BaudRateBPS = 0,
                                           .CharFormat  = CDC_LINEENCODING_OneStopBit,
                                           .ParityType  = CDC_PARITY_None,
                                           .DataBits    = 8                            };

/* timer/counter0 compare interrupt */
ISR(TIMER0_COMPA_vect)
{

   if(count == 255) {
      count =0; 
      if(led1r > 0) { SET_1R; }
      if(led1g > 0) { SET_1G; }
      if(led1b > 0) { SET_1B; }
      if(led2r > 0) { SET_2R; }
      if(led2g > 0) { SET_2G; }
      if(led2b > 0) { SET_2B; }
      if(led3r > 0) { SET_3R; }
      if(led3g > 0) { SET_3G; }
      if(led3b > 0) { SET_3B; }
      if(led4r > 0) { SET_4R; }
      if(led4g > 0) { SET_4G; }
      if(led4b > 0) { SET_4B; }
   }
   else {
      /* check how long led should be on */
      if(count == led1r) { CLR_1R; }
      if(count == led1g) { CLR_1G; }
      if(count == led1b) { CLR_1B; }
      if(count == led2r) { CLR_2R; }
      if(count == led2g) { CLR_2G; }
      if(count == led2b) { CLR_2B; }
      if(count == led3r) { CLR_3R; }
      if(count == led3g) { CLR_3G; }
      if(count == led3b) { CLR_3B; }
      if(count == led4r) { CLR_4R; }
      if(count == led4g) { CLR_4G; }
      if(count == led4b) { CLR_4B; }
      count++;
   }
}

/* flash all leds on and off*/
void flash_all(void)
{
   SET_1R;
   _delay_ms(500.0);
   CLR_1R;
   SET_1G;
   _delay_ms(500.0);
   CLR_1G;
   SET_1B;
   _delay_ms(500.0);
   CLR_1B;

   SET_2R;
   _delay_ms(500.0);
   CLR_2R;
   SET_2G;
   _delay_ms(500.0);
   CLR_2G;
   SET_2B;
   _delay_ms(500.0);
   CLR_2B;

   SET_3R;
   _delay_ms(500.0);
   CLR_3R;
   SET_3G;
   _delay_ms(500.0);
   CLR_3G;
   SET_3B;
   _delay_ms(500.0);
   CLR_3B;

   SET_4R;
   _delay_ms(500.0);
   CLR_4R;
   SET_4G;
   _delay_ms(500.0);
   CLR_4G;
   SET_4B;
   _delay_ms(500.0);
   CLR_4B;
}


void SetupHardware(void)
{
   /* Disable watchdog if enabled by bootloader/fuses */
   MCUSR &= ~(1 << WDRF);
   wdt_disable();

   /* Disable clock division */
   clock_prescale_set(clock_div_1);

   DDRD = 0xff; // port D output
   DDRB = 0xff; // port B output

   // init timer 0
    TCCR0A |= (1<<WGM01); // CTC
   TCCR0B |= (1<<CS01 | 1<<CS00); // scale 64
   TIMSK0  |= (1<<OCIE0A); // enable output compare int a
   OCR0A =  16;
 
   /* Hardware Initialization */
   //LEDs_Init();
   USB_Init();
}

/** Event handler for the USB_Connect event. This indicates that the device is enumerating via the status LEDs and
 *  starts the library USB task to begin the enumeration and USB management process.
 */
void EVENT_USB_Device_Connect(void)
{
   /* Indicate USB enumerating */
   //LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
   SET_1B;
   CLR_1R;
   CLR_1G;
}

/** Event handler for the USB_Disconnect event. This indicates that the device is no longer connected to a host via
 *  the status LEDs and stops the USB management and CDC management tasks.
 */
void EVENT_USB_Device_Disconnect(void)
{
   /* Indicate USB not ready */
   //LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
   SET_1R;
   CLR_1B;
   CLR_1G;

}

/** Event handler for the USB_ConfigurationChanged event. This is fired when the host set the current configuration
 *  of the USB device after enumeration - the device endpoints are configured and the CDC management task started.
 */
void EVENT_USB_Device_ConfigurationChanged(void)
{

   /* Setup CDC Data Endpoints */
   Endpoint_ConfigureEndpoint(CDC_NOTIFICATION_EPNUM, EP_TYPE_INTERRUPT, ENDPOINT_DIR_IN,
					       CDC_NOTIFICATION_EPSIZE, ENDPOINT_BANK_SINGLE);
   Endpoint_ConfigureEndpoint(CDC_TX_EPNUM, EP_TYPE_BULK, ENDPOINT_DIR_IN,
					       CDC_TXRX_EPSIZE, ENDPOINT_BANK_SINGLE);
   Endpoint_ConfigureEndpoint(CDC_RX_EPNUM, EP_TYPE_BULK, ENDPOINT_DIR_OUT,
					       CDC_TXRX_EPSIZE, ENDPOINT_BANK_SINGLE);
   
   /* Reset line encoding baud rate so that the host knows to send new values */
   LineEncoding.BaudRateBPS = 0;
   
}


/** Event handler for the USB_ControlRequest event. This is used to catch and process control requests sent to
 *  the device from the USB host before passing along unhandled control requests to the library for processing
 *  internally.
 */
void EVENT_USB_Device_ControlRequest(void)
{
   /* Process CDC specific control requests */
   switch (USB_ControlRequest.bRequest)
   {
   case CDC_REQ_GetLineEncoding:
      if (USB_ControlRequest.bmRequestType == (REQDIR_DEVICETOHOST | REQTYPE_CLASS | REQREC_INTERFACE))
      {
	 Endpoint_ClearSETUP();
	 
	 /* Write the line coding data to the control endpoint */
	 Endpoint_Write_Control_Stream_LE(&LineEncoding, sizeof(CDC_LineEncoding_t));
	 Endpoint_ClearOUT();
      }
      
      break;
   case CDC_REQ_SetLineEncoding:
      if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE))
      {
	 Endpoint_ClearSETUP();
	 
	 /* Read the line coding data in from the host into the global struct */
	 Endpoint_Read_Control_Stream_LE(&LineEncoding, sizeof(CDC_LineEncoding_t));
	 Endpoint_ClearIN();
      }
      
                        break;
   case CDC_REQ_SetControlLineState:
      if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE))
      {
	 Endpoint_ClearSETUP();
	 Endpoint_ClearStatusStage();
	 
	 /* NOTE: Here you can read in the line state mask from the host, to get the current state of the output handshake
	    lines. The mask is read in from the wValue parameter in USB_ControlRequest, and can be masked against the
	    CONTROL_LINE_OUT_* masks to determine the RTS and DTR line states using the following code:
	 */
      }
      
      break;
   }
}


/** Function to manage CDC data transmission and reception to and from the host. */
void CDC_Task(void)
{
        
   /* Device must be connected and configured for the task to run */
   if (USB_DeviceState != DEVICE_STATE_Configured)
      return;

   /* Select the Serial Tx Endpoint */
   Endpoint_SelectEndpoint(CDC_TX_EPNUM);
   /* Finalize the stream transfer to send the last packet */
   Endpoint_ClearIN();


   /* Select the Serial Rx Endpoint */
   Endpoint_SelectEndpoint(CDC_RX_EPNUM);

   /* Check if endpoint has a command in it sent from the host */
   if (Endpoint_IsOUTReceived())
   {    
      uint16_t DataLength = Endpoint_BytesInEndpoint();

      if(DataLength == 12) { /* read RGB data */
	 uint8_t  Buffer[Endpoint_BytesInEndpoint()];
	 /* Read in the incoming packet into the buffer */
	 Endpoint_Read_Stream_LE(&Buffer, DataLength, NULL);
      led1r = Buffer[0];
      led1g = Buffer[1];
      led1b = Buffer[2];
      led2r = Buffer[3];
      led2g = Buffer[4];
      led2b = Buffer[5];
      led3r = Buffer[6];
      led3g = Buffer[7];
      led3b = Buffer[8];
     led4r = Buffer[9];
      led4g = Buffer[10];
      led4b = Buffer[11];

      } else { /* unknown data, discard */
	 Endpoint_Discard_Stream(DataLength, NULL);
      }
      
      /* Finalize the stream transfer to send the last packet */
      Endpoint_ClearOUT();
      
   }

}

int main(void)
{

   /*
   led1r=127;
   led2r=16;
   _delay_ms(100.0);
   led3r=255;
   led4r=255;
   _delay_ms(100.0);
   while(1) {};
   */
   SetupHardware();
   sei();

   for (;;)
   {
      CDC_Task();
      USB_USBTask();
   }



}
